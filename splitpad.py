#!/usr/bin/env python

import os
import sys
import math

from binascii import hexlify as _hexlify

idBytes = 4
size = 1024

def usage():
  print 'Usage:'
  print '	' + sys.argv[0] + ' <infile> <out directory>'
  sys.exit(1)

def check_files(infile, keydir):
  if not os.path.isfile(infile):
    print 'No such file', infile
    return False
  elif not os.path.isdir(keydir):
    print 'No such directory', keydir
    return False
  return True

def seedtorand(seed):
  # http://us1.php.net/manual/en/function.openssl-random-pseudo-bytes.php#104322
  global idBytes
  log    = math.log(idBytes, 2)
  bits   = int(log) + 1
  filter = int(1 << bits) - 1
  seed   = seed & filter
  if seed > idBytes:
    return None
  return seed

def splitpad(infile, keydir):
  global idBytes, size
  if check_files(infile, keydir):
    i = open(infile, 'rb')
    finished = False
    while not finished:
      keyfile = _hexlify(i.read(idBytes))
      while os.path.isfile(keydir + os.sep + keyfile):
        print keyfile, 'already exists'
        keyfile = keyfile[2:] + _hexlify(i.read(1))
      if len(keyfile) < idBytes * 2:
        break
      offset = None
      while offset == None:
        seed = i.read(1)
        if seed == '':
          break
        else:
          seed = ord(seed)
          offset = seedtorand(seed)
      if offset == None:
        break
      o = open(keydir + os.sep + keyfile, 'wb')
      o.write(i.read(size - offset))
      o.close()
      if i.read(1):
        i.seek(-1,1)
      else:
        finished = True
    i.close()
    if os.path.isfile(keydir + os.sep + keyfile) and os.path.getsize(keydir + os.sep + keyfile) == 0:
      print 'deleting empty file', keyfile
      os.remove(keydir + os.sep + keyfile)
  else:
    sys.exit(1)

if __name__=='__main__':
  if len(sys.argv) == 3:
    infile = sys.argv[1]
    keydir = sys.argv[2].rstrip(os.sep)
    splitpad(infile, keydir)
  else:
    usage()